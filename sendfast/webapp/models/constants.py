#hub choices
Hub_Type_Name = (
    ('', 'None'),
    ('1', 'INSTAKART'),
    ('2', 'MYNTRA'),)

#Rider choices
TYPE_OF_RELATIONSHIP = (
        ('1', 'Parent'),
        ('2', 'Wife'),
        ('3', 'Sibling'),
        ('4', 'Friend'),
        ('5', 'Others'))

LANGUAGES = (
        ('1', 'English'),
        ('2', 'Hindi'),
        ('3', 'Telugu'),
        ('4', 'Urdu'),
        ('5', 'Tamil'),
        ('6', 'Malayalam'),
        ('7', 'Kannada'),)
TYPE_OF_CONTRACT = (
    ('1', 'Full Time'),
    ('2', 'Part Time'),
    ('3', 'Pay per Delivery'))

TYPE_OF_VEHICLE = (
    ('1', 'Two Wheeler'),
    ('2', 'Three Wheeler'),
    ('3', 'Four Wheeler'))
TYPE_OF_QUALIFICATION = (
    ('1', 'Upto 7th'),
    ('2', '10th'),
    ('3', '12th'),
    ('4', 'Graduate'),
    ('5', 'Post Graduate'))


#admin choices

TYPE_OF_RELATIONSHIP = (
        ('1', 'Parent'),
        ('2', 'Wife'),
        ('3', 'Sibling'),
        ('4', 'Friend'),
        ('5', 'Others'))

TYPE_OF_QUALIFICATION = (
        ('1', 'Upto 7th'),
        ('2', '10th'),
        ('3', '12th'),
        ('4', 'Graduate'),
        ('5', 'Post Graduate'))
LANGUAGES = (
        ('1', 'English'),
        ('2', 'Hindi'),
        ('3', 'Telugu'),
        ('4', 'Urdu'),
        ('5', 'Tamil'),
        ('6', 'Malayalam'),
        ('7', 'Kannada'),)
GENDER = (
        ('1', 'Male'),
        ('2', 'Female'),)
ADMIN_TYPE = (
        ('', 'None'),
        ('1', 'Hub Manager'),
        ('2', 'Area Manager'),
        ('3', 'Region Manager'),
        ('4', 'City Head'),
        ('5', 'State Head'),
        ('6', 'Central Team'),)

#order choices
ONLINE_PAYMENT = 1
CASH_ON_DELIVERY = 2

TYPE_OF_PAYMENT = (
	(ONLINE_PAYMENT, "ONLINE_PAYMENT"),
	(CASH_ON_DELIVERY, "CASH_ON_DELIVERY")
)

#timestamp choices
SHIPMENT_STATUS_REASON_CHOICE= (
    ('1','NULL'),
    ('2','Customer Not Responding'),
    ('3','Request for reschedule'),
    ('4','COD Not Ready'),
    ('5','Rejected'),
    ('6','Rejected OpenDelivery'),
    ('7','Incomplete Address'),
    ('8','Heavy Traffic'),
    ('9','Vehicle Breakdown'),
    ('10','Corresponding Pickup Rejected'),
    ('11','-'),
    ('12','Others'),
    )

RIDER_STATUS_CHOICES = (
    ('1','Accepted'),
    ('2','Rejected'),
    ('3','On the way'),
    ('4','Received Package'),
)

RESCHEDULE_CHOICES=(
    ('1','NULL'),
    ('2','Customer Not Responding'),
    ('3','COD Not Ready'),
    ('4','Incomplete Address'),
    ('5','Heavy Traffic'),
    ('6','Vehicle Breakdown'),
    ('7','Others'),
)

SHIPMENT_STATUS_CHOICES = (
    ('1','Delivered'),
    ('2','Not Delivered'),
    ('3','Rescheduled'),
    ('4','Cancelled/Rejected'),
    ('5','assigned'),
    ('6','Assigned'),
    ('7','RiderNotAvailable'),
    ('8','Pending'),
    ('9','On The way')

)

CANCEL_CHOICES = (
    ('1','NULL'),
    ('2','Customer Not Responding'),
    ('3','Customer Rejected'),
    ('4','Others'),
)

DELIVERY_CHOICES = (
    ('1','Delivered'),
    ('2','Not Delivered'),
    ('3','Rescheduled'),
)

#wallet choices
ORDER_COMMISSION = "ORDER_COMMISSION"
RIDER_PAYMENT_COLLECTION = "RIDER_PAYMENT"

TRANSACTION_TYPES = (
	(ORDER_COMMISSION, ORDER_COMMISSION),
	(RIDER_PAYMENT_COLLECTION, RIDER_PAYMENT_COLLECTION)
)

