from django.contrib import admin

# Register your models here.
from sf_admin.models.sf_admin import Admin_User
from sf_admin.models.sf_admin_addtional_fields import Admin_Additional

admin.site.register(Admin_User)
admin.site.register(Admin_Additional)