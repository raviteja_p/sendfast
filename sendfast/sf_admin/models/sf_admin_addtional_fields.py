from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from django.core.validators import RegexValidator
from webapp.models.constants import TYPE_OF_QUALIFICATION, LANGUAGES, GENDER
# from admin import Admin_User
from sf_admin import Admin_User


class Admin_Additional(AuditBasic):
    admin_data = models.OneToOneField(Admin_User, verbose_name='Admin Associated')
    phone_regex = RegexValidator(regex=r'^\+?([0,7,8,9]{1})}?\d{9,11}$',
                                 message="Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")
    Admin_perm_address = models.TextField(max_length=1000, verbose_name='Permanent Address', blank=True)
    guardian_address = models.TextField(max_length=1000, verbose_name='Guardian Address', blank=True)
    admin_password = models.CharField(max_length=100, blank=False, null=False, verbose_name='Admin Password')
    Admin_is_active_flag = models.BooleanField(default=True, verbose_name='Is Admin Active In Last 30 Days?')
    Admin_qualification = models.CharField(max_length=2, choices=TYPE_OF_QUALIFICATION,
                                           verbose_name='ACADEMIC QUALIFICATION', blank=True)
    Admin_alternative_number = models.CharField(validators=[phone_regex], blank=True, null=False, max_length=10,
                                                verbose_name='Rider Alternative Number')
    languages_known = models.CharField(max_length=3, choices=LANGUAGES, verbose_name='LANGUAGES KNOWN', blank=True)
    maritial_status = models.BooleanField(default=False, verbose_name='Married?')
    Admin_email = models.EmailField(blank=True)
    Admin_gender = models.CharField(max_length=2, choices=GENDER,
                                    verbose_name='Gender', blank=False, default="1")
    date_of_birth = models.DateField(blank=True, null=True)
    work_experience = models.PositiveIntegerField(default=0)
