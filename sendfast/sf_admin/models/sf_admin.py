from django.db import  models
from django.core.validators import RegexValidator
from datetime import date
from webapp.models.constants import TYPE_OF_RELATIONSHIP,ADMIN_TYPE
from sfcore.models.AuditBasic import AuditBasic
class Admin_User(AuditBasic):
    admin_user_id = models.AutoField(primary_key=True, db_index=True, verbose_name='Admin ID')
    Admin_Name = models.CharField(max_length=120, blank=False, null=False, unique=False, verbose_name='Employee Name')
    phone_regex = RegexValidator(regex=r'^\+?([0,7,8,9]{1})}?\d{9,11}$',
                                 message="Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")
    Admin_phone_number = models.CharField(validators=[phone_regex], blank=False, null=False, max_length=10,
                                          verbose_name='Admin Phone Number')
    Admin_Type = models.CharField(max_length=2, choices=ADMIN_TYPE, verbose_name='DESIGNATION', blank=True)
    joining_date = models.DateField(("Joining Date"), default=date.today)
    #guardian details
    guardian_Name = models.CharField(max_length=120, unique=False, verbose_name='Guardian Name', blank=True)
    guardian_relationship = models.CharField(max_length=2, choices=TYPE_OF_RELATIONSHIP,verbose_name='Type of Relationship ?', blank=True)
    guardian_contact_number = models.CharField(validators=[phone_regex], max_length=10,verbose_name='Guardian Phone Number', blank=True)
    guardian_alternative_number = models.CharField(validators=[phone_regex], max_length=10,verbose_name='Guardian Alternative Number', blank=True)
    Admin_AADHAR_number = models.CharField(max_length=100, verbose_name='AADHAR Number', blank=True)
    Admin_PAN_CARD_number = models.CharField(max_length=100, verbose_name='PAN CARD Number', blank=True)
    # user_added = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)

    class Meta:
        verbose_name_plural="Sendfast Admin"

    def __unicode__(self):
        return self.Admin_Name

