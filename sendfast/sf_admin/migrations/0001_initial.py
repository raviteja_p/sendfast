# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin_Additional',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('Admin_perm_address', models.TextField(max_length=1000, verbose_name=b'Permanent Address', blank=True)),
                ('guardian_address', models.TextField(max_length=1000, verbose_name=b'Guardian Address', blank=True)),
                ('admin_password', models.CharField(max_length=100, verbose_name=b'Admin Password')),
                ('Admin_is_active_flag', models.BooleanField(default=True, verbose_name=b'Is Admin Active In Last 30 Days?')),
                ('Admin_qualification', models.CharField(blank=True, max_length=2, verbose_name=b'ACADEMIC QUALIFICATION', choices=[(b'1', b'Upto 7th'), (b'2', b'10th'), (b'3', b'12th'), (b'4', b'Graduate'), (b'5', b'Post Graduate')])),
                ('Admin_alternative_number', models.CharField(blank=True, max_length=10, verbose_name=b'Rider Alternative Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,11}$', message=b"Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")])),
                ('languages_known', models.CharField(blank=True, max_length=3, verbose_name=b'LANGUAGES KNOWN', choices=[(b'1', b'English'), (b'2', b'Hindi'), (b'3', b'Telugu'), (b'4', b'Urdu'), (b'5', b'Tamil'), (b'6', b'Malayalam'), (b'7', b'Kannada')])),
                ('maritial_status', models.BooleanField(default=False, verbose_name=b'Married?')),
                ('Admin_email', models.EmailField(max_length=254, blank=True)),
                ('Admin_gender', models.CharField(default=b'1', max_length=2, verbose_name=b'Gender', choices=[(b'1', b'Male'), (b'2', b'Female')])),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('work_experience', models.PositiveIntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Admin_User',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('admin_user_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Admin ID', primary_key=True)),
                ('Admin_Name', models.CharField(max_length=120, verbose_name=b'Employee Name')),
                ('Admin_phone_number', models.CharField(max_length=10, verbose_name=b'Admin Phone Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,11}$', message=b"Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")])),
                ('Admin_Type', models.CharField(blank=True, max_length=2, verbose_name=b'DESIGNATION', choices=[(b'', b'None'), (b'1', b'Hub Manager'), (b'2', b'Area Manager'), (b'3', b'Region Manager'), (b'4', b'City Head'), (b'5', b'State Head'), (b'6', b'Central Team')])),
                ('joining_date', models.DateField(default=datetime.date.today, verbose_name=b'Joining Date')),
                ('guardian_Name', models.CharField(max_length=120, verbose_name=b'Guardian Name', blank=True)),
                ('guardian_relationship', models.CharField(blank=True, max_length=2, verbose_name=b'Type of Relationship ?', choices=[(b'1', b'Parent'), (b'2', b'Wife'), (b'3', b'Sibling'), (b'4', b'Friend'), (b'5', b'Others')])),
                ('guardian_contact_number', models.CharField(blank=True, max_length=10, verbose_name=b'Guardian Phone Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,11}$', message=b"Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")])),
                ('guardian_alternative_number', models.CharField(blank=True, max_length=10, verbose_name=b'Guardian Alternative Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,11}$', message=b"Phone number must be entered in the format: '9848281223'. Only 10 digits allowed.")])),
                ('Admin_AADHAR_number', models.CharField(max_length=100, verbose_name=b'AADHAR Number', blank=True)),
                ('Admin_PAN_CARD_number', models.CharField(max_length=100, verbose_name=b'PAN CARD Number', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Employees',
            },
        ),
        migrations.AddField(
            model_name='admin_additional',
            name='admin_data',
            field=models.OneToOneField(verbose_name=b'Admin Associated', to='sf_admin.Admin_User'),
        ),
    ]
