# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sf_admin', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='admin_user',
            options={'verbose_name_plural': 'Sendfast Admin'},
        ),
    ]
