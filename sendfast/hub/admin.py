from django.contrib import admin

# Register your models here.
from hub.models.city import City
from hub.models.hub import Hub
from hub.models.pincode import Pincode
from hub.models.region import Region
from hub.models.state import State
from hub.models.sub_region import Sub_Region

admin.site.register(State)
admin.site.register(City)
admin.site.register(Hub)
admin.site.register(Pincode)
admin.site.register(Region)
admin.site.register(Sub_Region)