# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('city_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'City ID', primary_key=True)),
                ('city_name', models.CharField(unique=True, max_length=120, verbose_name=b'City Name')),
                ('city_slug', models.SlugField(max_length=100, verbose_name=b'City Slug', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Hub',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('hub_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Hub ID', primary_key=True)),
                ('hub_name', models.CharField(unique=True, max_length=120, verbose_name=b'Hub Name')),
                ('Corresponding_ID', models.CharField(max_length=120, null=True, blank=True)),
                ('hub_Type', models.CharField(blank=True, max_length=2, null=True, verbose_name=b'Type of Hub', choices=[(b'', b'None'), (b'1', b'INSTAKART'), (b'2', b'MYNTRA')])),
                ('hub_slug', models.SlugField(verbose_name=b'Hub Slug', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Hubs',
            },
        ),
        migrations.CreateModel(
            name='Pincode',
            fields=[
                ('pincode_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Pincode ID', primary_key=True)),
                ('pincode_num', models.IntegerField(default=50, verbose_name=b'Pincode Number')),
                ('pincode_slug', models.SlugField(verbose_name=b'Pincode Slug', blank=True)),
                ('related_hub', models.ForeignKey(verbose_name=b'Connected Hub', to='hub.Hub')),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('region_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Region ID', primary_key=True)),
                ('region_name', models.CharField(unique=True, max_length=120, verbose_name=b'Region Name')),
                ('region_slug', models.SlugField(max_length=100, verbose_name=b'Region Slug', blank=True)),
                ('city', models.ForeignKey(verbose_name=b'City', to='hub.City')),
            ],
            options={
                'verbose_name_plural': 'Regions',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('state_name', models.CharField(unique=True, max_length=120, verbose_name=b'State Name')),
                ('state_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'State ID', primary_key=True)),
                ('state_slug', models.SlugField(verbose_name=b'State slug', blank=True)),
            ],
            options={
                'verbose_name_plural': 'States',
            },
        ),
        migrations.CreateModel(
            name='Sub_Region',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('sub_region_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Sub Region ID', primary_key=True)),
                ('sub_region_name', models.CharField(unique=True, max_length=120, verbose_name=b'Sub Region Name')),
                ('sub_region_slug', models.SlugField(max_length=100, verbose_name=b'Sub Region Slug', blank=True)),
                ('region', models.ForeignKey(verbose_name=b'Region', to='hub.Region')),
            ],
            options={
                'verbose_name_plural': 'Sub Regions',
            },
        ),
        migrations.AddField(
            model_name='hub',
            name='sub_region',
            field=models.ForeignKey(verbose_name=b'Sub Region', to='hub.Sub_Region'),
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(verbose_name=b'State', to='hub.State'),
        ),
    ]
