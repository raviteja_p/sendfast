from django.db import  models
from sfcore.models.AuditBasic import  AuditBasic
from sub_region import Sub_Region
from webapp.models.constants import Hub_Type_Name
from django.template.defaultfilters import slugify

class Hub(AuditBasic):
    hub_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Hub ID')
    hub_name = models.CharField(max_length=120, blank=False, null=False, unique=True, verbose_name='Hub Name')
    Corresponding_ID = models.CharField(max_length=120, blank=True, null=True)
    hub_Type = models.CharField(max_length=2, choices=Hub_Type_Name,
                                verbose_name='Type of Hub', blank=True, null=True)
    hub_slug = models.SlugField(max_length=50,blank=True,verbose_name='Hub Slug')
    sub_region = models.ForeignKey(Sub_Region, verbose_name='Sub Region', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Hubs"

    def __unicode__(self):
        return self.hub_name

    def save(self,*args,**kwargs):
        self.hub_slug = slugify(self.hub_name)
        super(Hub,self).save(*args,**kwargs)

