from django.db import models
from sfcore.models.AuditBasic import  AuditBasic
from city import City
from django.template.defaultfilters import slugify

class Region(AuditBasic):
    region_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Region ID')
    region_name = models.CharField(max_length=120, blank=False, null=False, unique=True, verbose_name='Region Name')
    city = models.ForeignKey(City,verbose_name='City',on_delete=models.CASCADE)
    region_slug = models.SlugField(max_length=100,blank=True,verbose_name='Region Slug')

    class Meta:
        verbose_name_plural = "Regions"

    def __unicode__(self):
        return self.region_name

    def save(self, *args,**kwargs):
        self.region_slug = slugify(self.region_name)
        super(Region,self).save(*args,**kwargs)


