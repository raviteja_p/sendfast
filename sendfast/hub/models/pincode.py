from django.db import  models
from hub import Hub
from django.template.defaultfilters import slugify

class Pincode(models.Model):
    pincode_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Pincode ID')
    pincode_num = models.IntegerField(default=50,blank=False,verbose_name='Pincode Number')
    pincode_slug = models.SlugField(max_length=50,blank=True,verbose_name='Pincode Slug')
    related_hub = models.ForeignKey(Hub,verbose_name='Connected Hub', on_delete=models.CASCADE)


    def __unicode__(self):
        return str(self.pincode_num)

    def save(self,*args,**kwargs):
        self.pincode_slug = slugify(self.pincode_num)
        super(Pincode,self).save(*args,**kwargs)

