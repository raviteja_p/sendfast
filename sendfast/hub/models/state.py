from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from django.template.defaultfilters import slugify


class State(AuditBasic):
    state_name = models.CharField(max_length=120, blank=False, null=False,unique=True,verbose_name='State Name')
    state_id = models.AutoField(primary_key=True,db_index=True,verbose_name='State ID')
    state_slug = models.SlugField(max_length=50,blank=True,verbose_name='State slug')
    class Meta:
        verbose_name_plural = 'States'

    def __unicode__(self):
        return self.state_name

    def save(self,*args,**kwargs):
        self.state_slug = slugify(self.state_name)
        super(State,self).save(*args,**kwargs)


