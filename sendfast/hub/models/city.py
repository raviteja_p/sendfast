from django.db import models
from sfcore.models.AuditBasic import  AuditBasic
from django.template.defaultfilters import slugify
from state import State


class City(AuditBasic):
    city_id = models.AutoField(primary_key=True,db_index=True,verbose_name='City ID')
    city_name = models.CharField(max_length=120, blank=False, null=False, unique=True, verbose_name='City Name')
    state = models.ForeignKey(State, verbose_name='State', on_delete=models.CASCADE)
    city_slug = models.SlugField(max_length=100,blank=True,verbose_name='City Slug')

    class Meta:
        verbose_name_plural = "Cities"

    def __unicode__(self):
        return self.city_name

    def save(self,*args,**kwargs):
        self.city_slug = slugify(self.city_name)
        super(City,self).save(*args,**kwargs)