from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from region import Region
from django.template.defaultfilters import slugify

class Sub_Region(AuditBasic):
    sub_region_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Sub Region ID')
    sub_region_name = models.CharField(max_length=120, blank=False, null=False, unique=True,verbose_name='Sub Region Name')
    region = models.ForeignKey(Region, verbose_name='Region',on_delete=models.CASCADE)
    sub_region_slug = models.SlugField(max_length=100,blank=True,verbose_name='Sub Region Slug')

    class Meta:
        verbose_name_plural = "Sub Regions"

    def __unicode__(self):
        return self.sub_region_name

    def save(self, *args, **kwargs):
        self.sub_region_slug = slugify(self.sub_region_name)
        super(Sub_Region,self).save(*args,**kwargs)