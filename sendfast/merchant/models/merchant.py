from django.db import  models
from django.core.validators import RegexValidator
from sfcore.models.AuditBasic import AuditBasic
from hub.models.pincode import Pincode
from sf_admin.models.sf_admin import Admin_User


class Merchant(AuditBasic):
    admin_contact_name = models.ForeignKey(Admin_User,on_delete=models.CASCADE,verbose_name='Admin Contact')
    merchant_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Merchant ID')
    merchant_name = models.CharField(max_length=100,null=False,blank=False,db_index=True,verbose_name='Merchant Name')
    phone_regex = RegexValidator(regex=r'^\+?([0,7,8,9]{1})}?\d{9,12}$',message="Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")
    merchant_phone_number = models.CharField(validators=[phone_regex],blank=False,null=False,max_length=10,verbose_name='Merchants phone number')
    merchant_email = models.EmailField(null=False, blank=False, verbose_name='Merchant Email')
    merchant_gst_number = models.CharField(blank=False,null=False,max_length=100,verbose_name='Merchant GST Number')
    pincodes = models.ForeignKey(Pincode,verbose_name='Nearest Pincodes to Merchant')


    def __unicode__(self):
        return u"%s-%s"%(self.merchant_id,self.merchant_name)


