from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from shipment.models.shipment import Shipment

class MerchantWalletSummary(AuditBasic):
    id = models.AutoField(primary_key=True,db_index=True,verbose_name='ID')
    shipment = models.OneToOneField(Shipment,db_index=True,verbose_name='Shipment')
    credit_amount = models.DecimalField(default=0, decimal_places=7, max_digits=15, blank=True, null=True,
                                        verbose_name="Credit amount")
    debit_amount = models.DecimalField(default=0, decimal_places=7, max_digits=15, blank=True, null=True,
                                       verbose_name="Debit amount")
