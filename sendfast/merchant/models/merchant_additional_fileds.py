from django.db import  models
from django.core.validators import RegexValidator
from merchant import Merchant
from address.models import Address
class MerchantExtraAttributes(Address):
    id = models.AutoField(primary_key=True,db_index=True)
    merchant_associated = models.OneToOneField(Merchant,verbose_name='Merchant Associated',on_delete=models.CASCADE)
    phone_regex = RegexValidator(regex=r'^\+?([0,7,8,9]{1})}?\d{9,12}$',
                                 message="Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")
    merchant_alternate_number = models.CharField(validators=[phone_regex],blank=False,null=False,max_length=10,verbose_name='Merchants Alternate phone number')





