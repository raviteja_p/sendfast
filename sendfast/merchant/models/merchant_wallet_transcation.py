from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from webapp.models.constants import TRANSACTION_TYPES
from rider.models.rider import Rider
from shipment.models.shipment import Shipment

class MerchantTranscation(AuditBasic):
    id = models.AutoField(primary_key=True,db_index=True,verbose_name='Merchant Transaction ID')
    rider = models.ForeignKey(Rider,verbose_name='Rider')
    shipment = models.ForeignKey(Shipment,verbose_name='Business Order',blank=True,null=True)
    amount = models.DecimalField(decimal_places=7, max_digits=10, verbose_name="Amount")
    transaction_type = models.CharField(choices=TRANSACTION_TYPES, max_length=60, verbose_name="Transaction Type")
    remarks = models.CharField(blank=False, max_length=100, null=False, verbose_name="Remarks")

    def __unicode__(self):
        return u'{}-{}-{}'.format(self.id, self.rider, self.transaction_type)
