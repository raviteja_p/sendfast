from django.contrib import admin

# Register your models here.
from merchant.models.merchant import Merchant
from merchant.models.merchant_additional_fileds import MerchantExtraAttributes
from merchant.models.merchant_wallet_summary import MerchantWalletSummary
from merchant.models.merchant_wallet_transcation import MerchantTranscation

admin.site.register(Merchant)
admin.site.register(MerchantExtraAttributes)
admin.site.register(MerchantWalletSummary)
admin.site.register(MerchantTranscation)