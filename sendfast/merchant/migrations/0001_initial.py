# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Merchant',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('merchant_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Merchant ID', primary_key=True)),
                ('merchant_name', models.CharField(max_length=100, verbose_name=b'Merchant Name', db_index=True)),
                ('merchant_phone_number', models.CharField(max_length=10, verbose_name=b'Merchants phone number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
                ('merchant_email', models.EmailField(max_length=254, verbose_name=b'Merchant Email')),
                ('merchant_gst_number', models.CharField(max_length=100, verbose_name=b'Merchant GST Number')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MerchantExtraAttributes',
            fields=[
                ('latitude', models.DecimalField(verbose_name=b'Latitude', max_digits=10, decimal_places=7)),
                ('longitude', models.DecimalField(verbose_name=b'Longitude', max_digits=10, decimal_places=7)),
                ('house_no', models.CharField(max_length=20, null=True, verbose_name=b'Flat/House No.', blank=True)),
                ('apartment', models.CharField(max_length=100, null=True, verbose_name=b'Apartment / Locality name:', blank=True)),
                ('sub_locality', models.CharField(max_length=300, null=True, verbose_name=b' Sub Locality', blank=True)),
                ('locality', models.CharField(max_length=300, null=True, verbose_name=b' Locality', blank=True)),
                ('city', models.CharField(max_length=30, null=True, verbose_name=b' City', blank=True)),
                ('state', models.CharField(max_length=30, null=True, verbose_name=b'State', blank=True)),
                ('country', models.CharField(max_length=30, null=True, verbose_name=b'Country', blank=True)),
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('merchant_alternate_number', models.CharField(max_length=10, verbose_name=b'Merchants Alternate phone number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MerchantTranscation',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Merchant Transaction ID', primary_key=True)),
                ('amount', models.DecimalField(verbose_name=b'Amount', max_digits=10, decimal_places=7)),
                ('transaction_type', models.CharField(max_length=60, verbose_name=b'Transaction Type', choices=[(b'ORDER_COMMISSION', b'ORDER_COMMISSION'), (b'RIDER_PAYMENT', b'RIDER_PAYMENT')])),
                ('remarks', models.CharField(max_length=100, verbose_name=b'Remarks')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MerchantWalletSummary',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, verbose_name=b'ID', primary_key=True)),
                ('credit_amount', models.DecimalField(decimal_places=7, default=0, max_digits=15, blank=True, null=True, verbose_name=b'Credit amount')),
                ('debit_amount', models.DecimalField(decimal_places=7, default=0, max_digits=15, blank=True, null=True, verbose_name=b'Debit amount')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
