# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shipment', '0001_initial'),
        ('merchant', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='merchantwalletsummary',
            name='shipment',
            field=models.OneToOneField(verbose_name=b'Shipment', to='shipment.Shipment'),
        ),
    ]
