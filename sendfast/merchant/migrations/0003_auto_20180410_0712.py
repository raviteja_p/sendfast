# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sf_admin', '0001_initial'),
        ('hub', '0001_initial'),
        ('rider', '0001_initial'),
        ('shipment', '0001_initial'),
        ('merchant', '0002_merchantwalletsummary_shipment'),
    ]

    operations = [
        migrations.AddField(
            model_name='merchanttranscation',
            name='rider',
            field=models.ForeignKey(verbose_name=b'Rider', to='rider.Rider'),
        ),
        migrations.AddField(
            model_name='merchanttranscation',
            name='shipment',
            field=models.ForeignKey(verbose_name=b'Business Order', blank=True, to='shipment.Shipment', null=True),
        ),
        migrations.AddField(
            model_name='merchantextraattributes',
            name='merchant_associated',
            field=models.OneToOneField(verbose_name=b'Merchant Associated', to='merchant.Merchant'),
        ),
        migrations.AddField(
            model_name='merchant',
            name='admin_contact_name',
            field=models.ForeignKey(verbose_name=b'Admin Contact', to='sf_admin.Admin_User'),
        ),
        migrations.AddField(
            model_name='merchant',
            name='pincodes',
            field=models.ForeignKey(verbose_name=b'Nearest Pincodes to Merchant', to='hub.Pincode'),
        ),
    ]
