from django.contrib import admin

# Register your models here.
from rider.models.rider import Rider
from rider.models.rider_additional_fields import RiderAdditionalAttributes
from rider.models.rider_wallet_summary import RiderWalletSummary
from rider.models.rider_wallet_transaction import RiderWalletTransaction

admin.site.register(Rider)
admin.site.register(RiderAdditionalAttributes)
admin.site.register(RiderWalletSummary)
admin.site.register(RiderWalletTransaction)