# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hub', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Rider',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('rider_id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Rider ID', primary_key=True)),
                ('rider_name', models.CharField(max_length=120, verbose_name=b'Rider Name', db_index=True)),
                ('rider_phone_number', models.CharField(max_length=10, verbose_name=b'Riders phone number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
                ('rider_alternative_number', models.CharField(blank=True, max_length=10, verbose_name=b'Rider Alternative Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
                ('rider_AADHAR_number', models.CharField(max_length=100, verbose_name=b'Rider Aadhar Number', blank=True)),
                ('rider_PAN_CARD_NUMBER', models.CharField(max_length=100, verbose_name=b'Rider PAN Number', blank=True)),
                ('Corresponding_ID', models.CharField(max_length=120, unique=True, null=True, blank=True)),
                ('guardian_Name', models.CharField(max_length=120, verbose_name=b'Guardian Name', blank=True)),
                ('guardian_relationship', models.CharField(blank=True, max_length=2, verbose_name=b'Type of Relationship ?', choices=[(b'1', b'Parent'), (b'2', b'Wife'), (b'3', b'Sibling'), (b'4', b'Friend'), (b'5', b'Others')])),
                ('guardian_contact_number', models.CharField(blank=True, max_length=10, verbose_name=b'Guardian Phone Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
                ('guardian_alternative_number', models.CharField(blank=True, max_length=10, verbose_name=b'Guardian Alternative Number', validators=[django.core.validators.RegexValidator(regex=b'^\\+?([0,7,8,9]{1})}?\\d{9,12}$', message=b"Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")])),
                ('joining_date', models.DateField(default=datetime.date.today, verbose_name=b'Joining Date')),
                ('rider_driving_licence_number', models.CharField(max_length=100, verbose_name=b'Rider Driving Licence Number', blank=True)),
                ('rider_payment', models.IntegerField(default=60, verbose_name=b'Rider Payment')),
                ('hub', models.ForeignKey(verbose_name=b'Hub Associated', to='hub.Hub')),
                ('user_added', models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Rider Data',
            },
        ),
    ]
