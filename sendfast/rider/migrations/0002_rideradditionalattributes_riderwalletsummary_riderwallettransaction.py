# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
        ('rider', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RiderAdditionalAttributes',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('rider_type_of_contract', models.CharField(blank=True, max_length=2, verbose_name=b'Type of Contract ?', choices=[(b'1', b'Full Time'), (b'2', b'Part Time'), (b'3', b'Pay per Delivery')])),
                ('work_experience', models.PositiveIntegerField(default=0)),
                ('maritial_status', models.BooleanField(default=False, verbose_name=b'Married?')),
                ('rider_perm_address', models.TextField(max_length=1000, verbose_name=b'Rider Permanent Address', blank=True)),
                ('rider_alt_address', models.TextField(max_length=1000, verbose_name=b'Rider Alternative Address', blank=True)),
                ('rider_vehicle_number', models.CharField(max_length=100, verbose_name=b'Rider vehicle Number', blank=True)),
                ('rider_vehicle_type', models.CharField(blank=True, max_length=2, verbose_name=b'Rider vehicle Type', choices=[(b'1', b'Two Wheeler'), (b'2', b'Three Wheeler'), (b'3', b'Four Wheeler')])),
                ('rider_vehicle_capacity', models.PositiveIntegerField(default=0)),
                ('rider_is_active_flag', models.BooleanField(default=True, verbose_name=b'Is Rider Active In Last 7 Days?')),
                ('rider_is_monthly_active_flag', models.BooleanField(default=True, verbose_name=b'Is Rider Active In Last 30 Days?')),
                ('rider_is_verified_flag', models.BooleanField(default=False, verbose_name=b'Is Rider Verified By Admin?')),
                ('rider_qualification', models.CharField(blank=True, max_length=2, verbose_name=b'ACADEMIC QUALIFICATION', choices=[(b'1', b'Upto 7th'), (b'2', b'10th'), (b'3', b'12th'), (b'4', b'Graduate'), (b'5', b'Post Graduate')])),
                ('rider_gender', models.CharField(default=b'1', max_length=2, verbose_name=b'Gender', choices=[(b'1', b'Male'), (b'2', b'Female')])),
                ('rider_email', models.EmailField(max_length=254, blank=True)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('languages_known', models.CharField(blank=True, max_length=3, verbose_name=b'LANGUAGES KNOWN', choices=[(b'1', b'English'), (b'2', b'Hindi'), (b'3', b'Telugu'), (b'4', b'Urdu'), (b'5', b'Tamil'), (b'6', b'Malayalam'), (b'7', b'Kannada')])),
                ('rider_data', models.OneToOneField(verbose_name=b'Rider', to='rider.Rider')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiderWalletSummary',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, verbose_name=b'ID', primary_key=True)),
                ('credit_amount', models.DecimalField(decimal_places=7, default=0, max_digits=15, blank=True, null=True, verbose_name=b'Credit amount')),
                ('debit_amount', models.DecimalField(decimal_places=7, default=0, max_digits=15, blank=True, null=True, verbose_name=b'Debit amount')),
                ('rider', models.OneToOneField(verbose_name=b'Rider', to='rider.Rider')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiderWalletTransaction',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Rider Wallet Transcation ID', primary_key=True)),
                ('amount', models.DecimalField(verbose_name=b'Amount', max_digits=10, decimal_places=7)),
                ('transaction_type', models.CharField(max_length=60, verbose_name=b'Transaction Type', choices=[(b'ORDER_COMMISSION', b'ORDER_COMMISSION'), (b'RIDER_PAYMENT', b'RIDER_PAYMENT')])),
                ('remarks', models.CharField(max_length=100, verbose_name=b'Remarks')),
                ('order', models.ForeignKey(verbose_name=b'Business Order', blank=True, to='order.BusinessOrder', null=True)),
                ('rider', models.ForeignKey(verbose_name=b'Rider', to='rider.Rider')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
