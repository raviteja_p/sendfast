from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from rider import Rider
class RiderWalletSummary(AuditBasic):
    id = models.AutoField(primary_key=True, db_index=True, verbose_name="ID")
    rider = models.OneToOneField(Rider, db_index=True, verbose_name="Rider")
    credit_amount = models.DecimalField(default=0, decimal_places=7, max_digits=15, blank=True, null=True,
                                        verbose_name="Credit amount")
    debit_amount = models.DecimalField(default=0, decimal_places=7, max_digits=15, blank=True, null=True,
                                       verbose_name="Debit amount")

    def __unicode__(self):
        return u'{}-{}'.format(self.id, self.rider)
