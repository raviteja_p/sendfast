from django.db import  models
from webapp.models.constants import TYPE_OF_CONTRACT,TYPE_OF_VEHICLE,TYPE_OF_QUALIFICATION,LANGUAGES,GENDER
from sfcore.models.AuditBasic import  AuditBasic
from rider import Rider
class RiderAdditionalAttributes(AuditBasic):
    id = models.AutoField(primary_key=True,db_index=True)
    rider_data = models.OneToOneField(Rider, verbose_name="Rider", on_delete=models.CASCADE)
    rider_type_of_contract = models.CharField(max_length=2, choices=TYPE_OF_CONTRACT,verbose_name='Type of Contract ?', blank=True)
    work_experience = models.PositiveIntegerField(default=0)
    maritial_status = models.BooleanField(default=False,verbose_name='Married?')
    rider_perm_address = models.TextField(max_length=1000, verbose_name='Rider Permanent Address', blank=True)
    rider_alt_address = models.TextField(max_length=1000, verbose_name='Rider Alternative Address', blank=True)
    rider_vehicle_number = models.CharField(max_length=100, verbose_name='Rider vehicle Number', blank=True)
    rider_vehicle_type = models.CharField(max_length=2, choices=TYPE_OF_VEHICLE,
                                          verbose_name='Rider vehicle Type', blank=True)
    rider_vehicle_capacity = models.PositiveIntegerField(default=0)
    rider_is_active_flag = models.BooleanField(default=True,verbose_name='Is Rider Active In Last 7 Days?')
    rider_is_monthly_active_flag = models.BooleanField(default=True,verbose_name='Is Rider Active In Last 30 Days?')
    rider_is_verified_flag = models.BooleanField(default=False,verbose_name='Is Rider Verified By Admin?')
    rider_qualification = models.CharField(max_length=2, choices=TYPE_OF_QUALIFICATION,
                                           verbose_name='ACADEMIC QUALIFICATION', blank=True)
    rider_gender = models.CharField(max_length=2, choices=GENDER, blank=False, default="1", verbose_name='Gender')
    rider_email = models.EmailField(blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    languages_known = models.CharField(max_length=3, choices=LANGUAGES, verbose_name='LANGUAGES KNOWN', blank=True)

