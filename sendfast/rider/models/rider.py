from django.db import  models
from django.conf import  settings
from webapp.models.constants import TYPE_OF_RELATIONSHIP,GENDER
from django.core.validators import RegexValidator
from datetime import  date
from hub.models.hub import Hub
from sfcore.models.AuditBasic import AuditBasic

class Rider(AuditBasic):
    phone_regex = RegexValidator(regex=r'^\+?([0,7,8,9]{1})}?\d{9,12}$',
                                 message="Phone number must be entered in the format: '9848281223'. Up to 11 digits allowed.")
    rider_id = models.AutoField(primary_key=True,db_index=True,verbose_name='Rider ID')
    rider_name = models.CharField(null=False,blank=False,db_index=True,unique=False,max_length=120,verbose_name='Rider Name')
    rider_phone_number = models.CharField(validators=[phone_regex],blank=False,null=False,max_length=10,verbose_name='Riders phone number')
    rider_alternative_number = models.CharField(validators=[phone_regex], blank=True, null=False, max_length=10,verbose_name='Rider Alternative Number')
    rider_AADHAR_number  = models.CharField(max_length=100,blank=True,verbose_name='Rider Aadhar Number')
    rider_PAN_CARD_NUMBER = models.CharField(max_length=100,blank=True,verbose_name='Rider PAN Number')

    Corresponding_ID = models.CharField(max_length=120, blank=True, null=True, unique=True)
    user_added = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    #Rider's Guardian attributes
    guardian_Name = models.CharField(max_length=120, unique=False, verbose_name='Guardian Name', blank=True)
    guardian_relationship = models.CharField(max_length=2, choices=TYPE_OF_RELATIONSHIP,
                                             verbose_name='Type of Relationship ?', blank=True)
    guardian_contact_number = models.CharField(validators=[phone_regex], max_length=10,
                                               verbose_name='Guardian Phone Number', blank=True)
    guardian_alternative_number = models.CharField(validators=[phone_regex], max_length=10,
                                                   verbose_name='Guardian Alternative Number', blank=True)

    joining_date = models.DateField(("Joining Date"), default=date.today)
    rider_driving_licence_number = models.CharField(max_length=100, verbose_name='Rider Driving Licence Number',
                                                    blank=True)
    hub = models.ForeignKey(Hub,verbose_name='Hub Associated',on_delete=models.CASCADE)
    rider_payment = models.IntegerField(default=60,verbose_name='Rider Payment')

    class Meta:
        verbose_name_plural = 'Rider Data'

    def __unicode__(self):
        return self.rider_name