from django.contrib import admin

# Register your models here.
from order.models.order import BusinessOrder
from order.models.timestamp import Timestamp

admin.site.register(BusinessOrder)
admin.site.register(Timestamp)