from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from django.utils import timezone
from rider.models.rider import Rider
from hub.models.hub import Hub
from shipment.models.shipment import Shipment

class BusinessOrder(AuditBasic):
    shipment_associated = models.ForeignKey(Shipment,verbose_name='Shipments Associated to Order',on_delete=models.CASCADE)
    order_id = models.AutoField(primary_key=True, db_index=True)
    is_active = models.BooleanField(default=False, verbose_name="Is Order Active")
    order_slug = models.CharField(editable=False, unique=True, max_length=40, db_index=True, verbose_name="order_slug")
    order_datetime = models.DateTimeField(default=timezone.now, verbose_name="Order Time")
    scheduled_datetime = models.DateTimeField(default=timezone.now, verbose_name="Scheduled Time")
    rider = models.ForeignKey(Rider, blank=True, null=True, verbose_name="Assigned Rider")
    is_reassigned = models.BooleanField(default=False, verbose_name="Is Rider reassigned")
    start_latitude = models.DecimalField(max_digits=10, blank=True, null=True, decimal_places=7,
                                         verbose_name="Rider Start Latitude")
    start_longitude = models.DecimalField(max_digits=10, blank=True, null=True, decimal_places=7,
                                          verbose_name="Rider Start Longitude")
    tracking_url = models.CharField(blank=True, max_length=300, verbose_name="Order Tracking Url")
    hub_name = models.ForeignKey(Hub,verbose_name='Connected Hub',on_delete=models.CASCADE)
    remarks = models.CharField(max_length=80, null=True, blank=True, verbose_name="Remarks")



    def __unicode__(self):
        return "%d"%(self.order_id)


