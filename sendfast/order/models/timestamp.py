from django.db import models
from sfcore.models.AuditBasic import AuditBasic
from order import BusinessOrder
from webapp.models.constants import RIDER_STATUS_CHOICES,SHIPMENT_STATUS_CHOICES,RESCHEDULE_CHOICES,CANCEL_CHOICES,DELIVERY_CHOICES

class Timestamp(AuditBasic):
    id = models.AutoField(primary_key=True,db_index=True,verbose_name='Timestamp ID')
    orders = models.ForeignKey(BusinessOrder,verbose_name='Orders associated',on_delete=models.CASCADE)
    assigned = models.DateTimeField(null=True,blank=True,verbose_name='Shipment Assigned time to Rider')
    rider_start_time =models.DateTimeField(null=True,blank=True,verbose_name="Rider Trip Start time")
    rider_updated_time = models.DateTimeField(null=True,blank=True,verbose_name='Rider Delivery Time')
    shipment_status = models.IntegerField(choices=SHIPMENT_STATUS_CHOICES,verbose_name='Shipment Status')
    rescheduled = models.IntegerField(choices=RESCHEDULE_CHOICES,verbose_name="Shipment/Order resheduled")
    cancelled = models.IntegerField(choices=CANCEL_CHOICES,verbose_name='Cancelled Choices')
    delivered = models.IntegerField(choices=DELIVERY_CHOICES,verbose_name='Delivery Choices')

