# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hub', '0001_initial'),
        ('rider', '0001_initial'),
        ('shipment', '0002_pickupaddress_shipmentdeliveryaddress'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessOrder',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('order_id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('is_active', models.BooleanField(default=False, verbose_name=b'Is Order Active')),
                ('order_slug', models.CharField(verbose_name=b'order_slug', unique=True, max_length=40, editable=False, db_index=True)),
                ('order_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'Order Time')),
                ('scheduled_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'Scheduled Time')),
                ('is_reassigned', models.BooleanField(default=False, verbose_name=b'Is Rider reassigned')),
                ('start_latitude', models.DecimalField(null=True, verbose_name=b'Rider Start Latitude', max_digits=10, decimal_places=7, blank=True)),
                ('start_longitude', models.DecimalField(null=True, verbose_name=b'Rider Start Longitude', max_digits=10, decimal_places=7, blank=True)),
                ('tracking_url', models.CharField(max_length=300, verbose_name=b'Order Tracking Url', blank=True)),
                ('remarks', models.CharField(max_length=80, null=True, verbose_name=b'Remarks', blank=True)),
                ('hub_name', models.ForeignKey(verbose_name=b'Connected Hub', to='hub.Hub')),
                ('rider', models.ForeignKey(verbose_name=b'Assigned Rider', blank=True, to='rider.Rider', null=True)),
                ('shipment_associated', models.ForeignKey(verbose_name=b'Shipments Associated to Order', to='shipment.Shipment')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Timestamp',
            fields=[
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(db_index=True, serialize=False, verbose_name=b'Timestamp ID', primary_key=True)),
                ('assigned', models.DateTimeField(null=True, verbose_name=b'Shipment Assigned time to Rider', blank=True)),
                ('rider_start_time', models.DateTimeField(null=True, verbose_name=b'Rider Trip Start time', blank=True)),
                ('rider_updated_time', models.DateTimeField(null=True, verbose_name=b'Rider Delivery Time', blank=True)),
                ('shipment_status', models.IntegerField(verbose_name=b'Shipment Status', choices=[(b'1', b'Delivered'), (b'2', b'Not Delivered'), (b'3', b'Rescheduled'), (b'4', b'Cancelled/Rejected'), (b'5', b'assigned'), (b'6', b'Assigned'), (b'7', b'RiderNotAvailable'), (b'8', b'Pending'), (b'9', b'On The way')])),
                ('rescheduled', models.IntegerField(verbose_name=b'Shipment/Order resheduled', choices=[(b'1', b'NULL'), (b'2', b'Customer Not Responding'), (b'3', b'COD Not Ready'), (b'4', b'Incomplete Address'), (b'5', b'Heavy Traffic'), (b'6', b'Vehicle Breakdown'), (b'7', b'Others')])),
                ('cancelled', models.IntegerField(verbose_name=b'Cancelled Choices', choices=[(b'1', b'NULL'), (b'2', b'Customer Not Responding'), (b'3', b'Customer Rejected'), (b'4', b'Others')])),
                ('delivered', models.IntegerField(verbose_name=b'Delivery Choices', choices=[(b'1', b'Delivered'), (b'2', b'Not Delivered'), (b'3', b'Rescheduled')])),
                ('orders', models.ForeignKey(verbose_name=b'Orders associated', to='order.BusinessOrder')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
