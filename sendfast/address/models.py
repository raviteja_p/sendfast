from django.db import models



class GeoAddress(models.Model):
    latitude = models.DecimalField(max_digits=10, decimal_places=7, verbose_name="Latitude")
    longitude = models.DecimalField(max_digits=10, decimal_places=7, verbose_name="Longitude")

    class Meta:
        abstract = True

    def get_lat_long(self):
        return (self.latitude, self.longitude)


class Address(GeoAddress):
    house_no = models.CharField(blank=True, null=True, max_length=20, verbose_name='Flat/House No.')
    apartment = models.CharField(blank=True, null=True, max_length=100, verbose_name='Apartment / Locality name:')
    sub_locality = models.CharField(blank=True, null=True, max_length=300, verbose_name=' Sub Locality')
    locality = models.CharField(blank=True, null=True, max_length=300, verbose_name=' Locality')
    city = models.CharField(blank=True, null=True, max_length=30, verbose_name=' City')
    state = models.CharField(blank=True, null=True, max_length=30, verbose_name="State")
    country = models.CharField(blank=True, null=True, max_length=30, verbose_name="Country")

    class Meta:
        abstract = True