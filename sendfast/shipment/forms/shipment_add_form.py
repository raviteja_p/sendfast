from django import forms
from shipment.models.shipment import Shipment
class ShipmentForm(forms.ModelForm):
    class Meta:
        model = Shipment
        fields = ('merchant_related','shipment_id','shipment_price','shipment_items','type_of_payment','remarks')



