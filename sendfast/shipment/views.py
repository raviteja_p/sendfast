from django.shortcuts import render
from django.shortcuts import render,get_object_or_404,HttpResponseRedirect
from shipment.forms.shipment_add_form import ShipmentForm
from shipment.models.shipment import Shipment



def shipment_list(request):
    shipments = Shipment.objects.all()
    return render(request,"shipment_list.html",{'shipments':shipments})

def shipment_detail(request,id=None):
    shipment_instance = get_object_or_404(Shipment,id=id)
    context = {
        'shipment':shipment_instance
    }
    return render(request,"shipment_detail.html",context)

def shipment_edit(request,id=None):
    instance = get_object_or_404(Shipment,id=id)
    form = ShipmentForm(request.POST or None,instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect('/shipment/list/%s/'%id)
    context = {'instance':instance,'form':form}
    return render(request,"shipment_form.html",context)

def addshipment(request):
    if request.method == 'POST':
        form = ShipmentForm(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/shipment/list/')
    else:
        form = ShipmentForm()

    return  render(request,"shipment_form.html",{'form':form})


def shipment_delete(request,id=None):
    instance = get_object_or_404(Shipment,id=id)
    instance.delete()
    return HttpResponseRedirect('/shipment/list/')

