from django.db import  models
from address.models import Address
from shipment import Shipment
from django.utils import timezone

class PickUpAddress(Address):
    id = models.AutoField(primary_key=True, db_index=True)
    order = models.OneToOneField(Shipment, verbose_name="Shipment associated",db_index=True)
    phone_number = models.CharField(max_length=80, blank=True, null=True, verbose_name="Phone Number")
    place_name = models.CharField(max_length=80, blank=True, null=True, verbose_name="Place Name")
    scheduled_datetime = models.DateTimeField(default=timezone.now, verbose_name="Scheduled Time")

    class Meta:
        verbose_name_plural = "Shipment Pickup Address"

    def __unicode__(self):
        return u'Pickup Address: {}-{}'.format(self.latitude, self.longitude)
