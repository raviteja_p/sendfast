from django.db import models
from merchant.models.merchant import Merchant
from sfcore.models.AuditBasic import AuditBasic
from webapp.models.constants import TYPE_OF_PAYMENT,CASH_ON_DELIVERY
from django.template.defaultfilters import slugify
class Shipment(AuditBasic):
    #shipment models
    merchant_related = models.ForeignKey(Merchant,on_delete=models.CASCADE,verbose_name="merchant related to shipment")

    shipment_id = models.CharField(max_length=100, null=True, blank=False, verbose_name='Shipment ID')
    shipment_slug = models.SlugField(max_length=50,blank=True,verbose_name='Shipment Slug')
    shipment_price = models.DecimalField(default=0, decimal_places=7, max_digits=15, blank=True, null=True,
                                        verbose_name="Shipment Price")
    shipment_tracking_url = models.CharField(blank=True,null=True, max_length=300, verbose_name="shipment Tracking Url")
    shipment_items = models.PositiveIntegerField(default=0,verbose_name='Number of shipment items')
    shipment_status = models.CharField(max_length=100, null=True, blank=False,
                                       verbose_name='Shipment Status whether received or not')
    type_of_payment = models.IntegerField(choices=TYPE_OF_PAYMENT, default=CASH_ON_DELIVERY,
                                          verbose_name='Type of Payment')
    remarks = models.CharField(max_length=80, null=True, blank=True, verbose_name="Remarks")

    def __unicode__(self):
        return "%s"%(self.merchant_related)

    def save(self,*args,**kwargs):
        self.shipment_slug = slugify(self.merchant_related)
        super(Shipment,self).save(*args,**kwargs)

