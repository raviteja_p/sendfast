from django.db import models
from address.models import Address
from shipment import Shipment
class ShipmentDeliveryAddress(Address):
    id = models.AutoField(db_index=True, primary_key=True)
    order = models.OneToOneField(Shipment,db_index=True,verbose_name="shipment associated")
    phone_number = models.CharField(blank=True, null=True, max_length=15, verbose_name="Deliver address phone number")
    person_name = models.CharField(max_length=40, default="Anonymous", blank=True, null=True,
                                   verbose_name="Person Name")

    class Meta:
        verbose_name_plural = "Shipment Deliver Address"

    def __unicode__(self):
        return u'Deliver Address: {}-{}'.format(self.latitude, self.longitude)

