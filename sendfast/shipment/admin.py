from django.contrib import admin

# Register your models here.
from shipment.models.shipment import Shipment
from shipment.models.shipment_delivery_address import ShipmentDeliveryAddress
from shipment.models.shipment_pickup_address import PickUpAddress

admin.site.register(Shipment)
admin.site.register(ShipmentDeliveryAddress)
admin.site.register(PickUpAddress)