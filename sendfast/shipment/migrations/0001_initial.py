# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('merchant', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Shipment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('updated_ts', models.DateTimeField(auto_now=True)),
                ('shipment_id', models.CharField(max_length=100, null=True, verbose_name=b'Shipment ID')),
                ('shipment_slug', models.SlugField(verbose_name=b'Shipment Slug', blank=True)),
                ('shipment_price', models.DecimalField(decimal_places=7, default=0, max_digits=15, blank=True, null=True, verbose_name=b'Shipment Price')),
                ('shipment_tracking_url', models.CharField(max_length=300, null=True, verbose_name=b'shipment Tracking Url', blank=True)),
                ('shipment_items', models.PositiveIntegerField(default=0, verbose_name=b'Number of shipment items')),
                ('shipment_status', models.CharField(max_length=100, null=True, verbose_name=b'Shipment Status whether received or not')),
                ('type_of_payment', models.IntegerField(default=2, verbose_name=b'Type of Payment', choices=[(1, b'ONLINE_PAYMENT'), (2, b'CASH_ON_DELIVERY')])),
                ('remarks', models.CharField(max_length=80, null=True, verbose_name=b'Remarks', blank=True)),
                ('merchant_related', models.ForeignKey(verbose_name=b'merchant related to shipment', to='merchant.Merchant')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
