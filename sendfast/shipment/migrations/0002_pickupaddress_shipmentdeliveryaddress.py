# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('shipment', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PickUpAddress',
            fields=[
                ('latitude', models.DecimalField(verbose_name=b'Latitude', max_digits=10, decimal_places=7)),
                ('longitude', models.DecimalField(verbose_name=b'Longitude', max_digits=10, decimal_places=7)),
                ('house_no', models.CharField(max_length=20, null=True, verbose_name=b'Flat/House No.', blank=True)),
                ('apartment', models.CharField(max_length=100, null=True, verbose_name=b'Apartment / Locality name:', blank=True)),
                ('sub_locality', models.CharField(max_length=300, null=True, verbose_name=b' Sub Locality', blank=True)),
                ('locality', models.CharField(max_length=300, null=True, verbose_name=b' Locality', blank=True)),
                ('city', models.CharField(max_length=30, null=True, verbose_name=b' City', blank=True)),
                ('state', models.CharField(max_length=30, null=True, verbose_name=b'State', blank=True)),
                ('country', models.CharField(max_length=30, null=True, verbose_name=b'Country', blank=True)),
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('phone_number', models.CharField(max_length=80, null=True, verbose_name=b'Phone Number', blank=True)),
                ('place_name', models.CharField(max_length=80, null=True, verbose_name=b'Place Name', blank=True)),
                ('scheduled_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'Scheduled Time')),
                ('order', models.OneToOneField(verbose_name=b'Shipment associated', to='shipment.Shipment')),
            ],
            options={
                'verbose_name_plural': 'Shipment Pickup Address',
            },
        ),
        migrations.CreateModel(
            name='ShipmentDeliveryAddress',
            fields=[
                ('latitude', models.DecimalField(verbose_name=b'Latitude', max_digits=10, decimal_places=7)),
                ('longitude', models.DecimalField(verbose_name=b'Longitude', max_digits=10, decimal_places=7)),
                ('house_no', models.CharField(max_length=20, null=True, verbose_name=b'Flat/House No.', blank=True)),
                ('apartment', models.CharField(max_length=100, null=True, verbose_name=b'Apartment / Locality name:', blank=True)),
                ('sub_locality', models.CharField(max_length=300, null=True, verbose_name=b' Sub Locality', blank=True)),
                ('locality', models.CharField(max_length=300, null=True, verbose_name=b' Locality', blank=True)),
                ('city', models.CharField(max_length=30, null=True, verbose_name=b' City', blank=True)),
                ('state', models.CharField(max_length=30, null=True, verbose_name=b'State', blank=True)),
                ('country', models.CharField(max_length=30, null=True, verbose_name=b'Country', blank=True)),
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('phone_number', models.CharField(max_length=15, null=True, verbose_name=b'Deliver address phone number', blank=True)),
                ('person_name', models.CharField(default=b'Anonymous', max_length=40, null=True, verbose_name=b'Person Name', blank=True)),
                ('order', models.OneToOneField(verbose_name=b'shipment associated', to='shipment.Shipment')),
            ],
            options={
                'verbose_name_plural': 'Shipment Deliver Address',
            },
        ),
    ]
