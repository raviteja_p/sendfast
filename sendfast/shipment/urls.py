from django.conf.urls import include, url
from views import addshipment,shipment_list,shipment_detail,shipment_edit,shipment_delete
urlpatterns = [
    url(r'^shipment-form/$',addshipment,name='shipment_form'),
    url(r'^list/$',shipment_list,name='shipment_list')  ,
    url(r'^list/(?P<id>\d+)/$',shipment_detail ,name = 'shipment_detail'),
    url(r'^list/(?P<id>\d+)/edit/$',shipment_edit ,name = 'shipment_edit'),
    url(r'^list/(?P<id>\d+)/delete/$',shipment_delete, name='shipment_delete'),
]
